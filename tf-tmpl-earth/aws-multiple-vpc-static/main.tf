# --- root/main.tf ---

# use the iperf image
data "aws_ami" "iperf_ami" {
  most_recent = true

  owners = ["314740337949"]

  filter {
    name   = "name"
    values = ["st3-iperf-image 2021-11-17T19-44-53.214Z"]
  }
}

# get list of az per region
data "aws_availability_zones" "azs" {
  state = "available"
}

# generate integer. used for names
resource "random_integer" "random" {
  min = 1
  max = 100
}

# specify the SSH key name and file location
resource "aws_key_pair" "st_auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

###############################################################
###############################################################
###############################################################
# 
# 
# VPC configs below
# seg9 maps to vpc9x with cidr "10.9x.0.0/yy"
#
#
###############################################################
###############################################################
###############################################################

###
### t91
###

### networking

# set vpc
resource "aws_vpc" "t91_vpc" {
  cidr_block           = var.t91_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t91_vpc_name}"
  }
}

# create IGW per VPC
resource "aws_internet_gateway" "t91_igw" {
  vpc_id = aws_vpc.t91_vpc.id

  tags = {
    Name = "st_igw-${var.t91_vpc_name}"
  }
}

# create RT for Public subnets
resource "aws_route_table" "t91_public_rt" {
  vpc_id = aws_vpc.t91_vpc.id

  tags = {
    Name = "st_public_rt_${var.t91}"
  }
}

# generate DF in public RT with NH IGW
resource "aws_route" "t91_default_route" {
  route_table_id         = aws_route_table.t91_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.t91_igw.id
}

# public subnet
resource "aws_subnet" "t91_public_sn" {
  vpc_id                  = aws_vpc.t91_vpc.id
  cidr_block              = cidrsubnet(var.t91_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t91}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t91_public_rt_assoc" {
  subnet_id      = aws_subnet.t91_public_sn.id
  route_table_id = aws_route_table.t91_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t91_private_rt" {
  vpc_id = aws_vpc.t91_vpc.id

  tags = {
    Name = "st_private_rt_${var.t91}"
  }
}

# private subnet
resource "aws_subnet" "t91_private_sn" {
  vpc_id                  = aws_vpc.t91_vpc.id
  cidr_block              = cidrsubnet(var.t91_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t91}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t91_private_rt_assoc" {
  subnet_id      = aws_subnet.t91_private_sn.id
  route_table_id = aws_route_table.t91_private_rt.id
}


### security group

resource "aws_security_group" "t91_public_sg" {

  name        = "t91_public_sg"
  vpc_id      = aws_vpc.t91_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t91}"
  }
}

### compute

resource "aws_instance" "t91_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t91_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t91}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t91_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t92
###

### networking

# set vpc
resource "aws_vpc" "t92_vpc" {
  cidr_block           = var.t92_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t92_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t92_igw" {
#   vpc_id = aws_vpc.t92_vpc.id

#   tags = {
#     Name = "st_igw-${var.t92_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t92_public_rt" {
  vpc_id = aws_vpc.t92_vpc.id

  tags = {
    Name = "st_public_rt_${var.t92}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t92_default_route" {
#   route_table_id         = aws_route_table.t92_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t92_igw.id
# }

# public subnet
resource "aws_subnet" "t92_public_sn" {
  vpc_id                  = aws_vpc.t92_vpc.id
  cidr_block              = cidrsubnet(var.t92_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t92}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t92_public_rt_assoc" {
  subnet_id      = aws_subnet.t92_public_sn.id
  route_table_id = aws_route_table.t92_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t92_private_rt" {
  vpc_id = aws_vpc.t92_vpc.id

  tags = {
    Name = "st_private_rt_${var.t92}"
  }
}

# private subnet
resource "aws_subnet" "t92_private_sn" {
  vpc_id                  = aws_vpc.t92_vpc.id
  cidr_block              = cidrsubnet(var.t92_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t92}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t92_private_rt_assoc" {
  subnet_id      = aws_subnet.t92_private_sn.id
  route_table_id = aws_route_table.t92_private_rt.id
}


### security group

resource "aws_security_group" "t92_public_sg" {

  name        = "t92_public_sg"
  vpc_id      = aws_vpc.t92_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t92}"
  }
}

### compute

resource "aws_instance" "t92_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t92_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t92}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t92_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t93
###

### networking

# set vpc
resource "aws_vpc" "t93_vpc" {
  cidr_block           = var.t93_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t93_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t93_igw" {
#   vpc_id = aws_vpc.t93_vpc.id

#   tags = {
#     Name = "st_igw-${var.t93_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t93_public_rt" {
  vpc_id = aws_vpc.t93_vpc.id

  tags = {
    Name = "st_public_rt_${var.t93}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t93_default_route" {
#   route_table_id         = aws_route_table.t93_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t93_igw.id
# }

# public subnet
resource "aws_subnet" "t93_public_sn" {
  vpc_id                  = aws_vpc.t93_vpc.id
  cidr_block              = cidrsubnet(var.t93_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t93}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t93_public_rt_assoc" {
  subnet_id      = aws_subnet.t93_public_sn.id
  route_table_id = aws_route_table.t93_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t93_private_rt" {
  vpc_id = aws_vpc.t93_vpc.id

  tags = {
    Name = "st_private_rt_${var.t93}"
  }
}

# private subnet
resource "aws_subnet" "t93_private_sn" {
  vpc_id                  = aws_vpc.t93_vpc.id
  cidr_block              = cidrsubnet(var.t93_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t93}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t93_private_rt_assoc" {
  subnet_id      = aws_subnet.t93_private_sn.id
  route_table_id = aws_route_table.t93_private_rt.id
}


### security group

resource "aws_security_group" "t93_public_sg" {

  name        = "t93_public_sg"
  vpc_id      = aws_vpc.t93_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t93}"
  }
}

### compute

resource "aws_instance" "t93_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t93_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t93}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t93_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t94
###

### networking

# set vpc
resource "aws_vpc" "t94_vpc" {
  cidr_block           = var.t94_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t94_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t94_igw" {
#   vpc_id = aws_vpc.t94_vpc.id

#   tags = {
#     Name = "st_igw-${var.t94_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t94_public_rt" {
  vpc_id = aws_vpc.t94_vpc.id

  tags = {
    Name = "st_public_rt_${var.t94}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t94_default_route" {
#   route_table_id         = aws_route_table.t94_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t94_igw.id
# }

# public subnet
resource "aws_subnet" "t94_public_sn" {
  vpc_id                  = aws_vpc.t94_vpc.id
  cidr_block              = cidrsubnet(var.t94_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t94}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t94_public_rt_assoc" {
  subnet_id      = aws_subnet.t94_public_sn.id
  route_table_id = aws_route_table.t94_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t94_private_rt" {
  vpc_id = aws_vpc.t94_vpc.id

  tags = {
    Name = "st_private_rt_${var.t94}"
  }
}

# private subnet
resource "aws_subnet" "t94_private_sn" {
  vpc_id                  = aws_vpc.t94_vpc.id
  cidr_block              = cidrsubnet(var.t94_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t94}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t94_private_rt_assoc" {
  subnet_id      = aws_subnet.t94_private_sn.id
  route_table_id = aws_route_table.t94_private_rt.id
}


### security group

resource "aws_security_group" "t94_public_sg" {

  name        = "t94_public_sg"
  vpc_id      = aws_vpc.t94_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t94}"
  }
}

### compute

resource "aws_instance" "t94_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t94_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t94}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t94_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t95
###

### networking

# set vpc
resource "aws_vpc" "t95_vpc" {
  cidr_block           = var.t95_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t95_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t95_igw" {
#   vpc_id = aws_vpc.t95_vpc.id

#   tags = {
#     Name = "st_igw-${var.t95_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t95_public_rt" {
  vpc_id = aws_vpc.t95_vpc.id

  tags = {
    Name = "st_public_rt_${var.t95}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t95_default_route" {
#   route_table_id         = aws_route_table.t95_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t95_igw.id
# }

# public subnet
resource "aws_subnet" "t95_public_sn" {
  vpc_id                  = aws_vpc.t95_vpc.id
  cidr_block              = cidrsubnet(var.t95_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t95}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t95_public_rt_assoc" {
  subnet_id      = aws_subnet.t95_public_sn.id
  route_table_id = aws_route_table.t95_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t95_private_rt" {
  vpc_id = aws_vpc.t95_vpc.id

  tags = {
    Name = "st_private_rt_${var.t95}"
  }
}

# private subnet
resource "aws_subnet" "t95_private_sn" {
  vpc_id                  = aws_vpc.t95_vpc.id
  cidr_block              = cidrsubnet(var.t95_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t95}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t95_private_rt_assoc" {
  subnet_id      = aws_subnet.t95_private_sn.id
  route_table_id = aws_route_table.t95_private_rt.id
}


### security group

resource "aws_security_group" "t95_public_sg" {

  name        = "t95_public_sg"
  vpc_id      = aws_vpc.t95_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t95}"
  }
}

### compute

resource "aws_instance" "t95_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t95_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t95}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t95_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t96
###

### networking

# set vpc
resource "aws_vpc" "t96_vpc" {
  cidr_block           = var.t96_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t96_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t96_igw" {
#   vpc_id = aws_vpc.t96_vpc.id

#   tags = {
#     Name = "st_igw-${var.t96_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t96_public_rt" {
  vpc_id = aws_vpc.t96_vpc.id

  tags = {
    Name = "st_public_rt_${var.t96}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t96_default_route" {
#   route_table_id         = aws_route_table.t96_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t96_igw.id
# }

# public subnet
resource "aws_subnet" "t96_public_sn" {
  vpc_id                  = aws_vpc.t96_vpc.id
  cidr_block              = cidrsubnet(var.t96_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t96}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t96_public_rt_assoc" {
  subnet_id      = aws_subnet.t96_public_sn.id
  route_table_id = aws_route_table.t96_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t96_private_rt" {
  vpc_id = aws_vpc.t96_vpc.id

  tags = {
    Name = "st_private_rt_${var.t96}"
  }
}

# private subnet
resource "aws_subnet" "t96_private_sn" {
  vpc_id                  = aws_vpc.t96_vpc.id
  cidr_block              = cidrsubnet(var.t96_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t96}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t96_private_rt_assoc" {
  subnet_id      = aws_subnet.t96_private_sn.id
  route_table_id = aws_route_table.t96_private_rt.id
}


### security group

resource "aws_security_group" "t96_public_sg" {

  name        = "t96_public_sg"
  vpc_id      = aws_vpc.t96_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t96}"
  }
}

### compute

resource "aws_instance" "t96_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t96_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t96}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t96_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t97
###

### networking

# set vpc
resource "aws_vpc" "t97_vpc" {
  cidr_block           = var.t97_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t97_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t97_igw" {
#   vpc_id = aws_vpc.t97_vpc.id

#   tags = {
#     Name = "st_igw-${var.t97_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t97_public_rt" {
  vpc_id = aws_vpc.t97_vpc.id

  tags = {
    Name = "st_public_rt_${var.t97}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t97_default_route" {
#   route_table_id         = aws_route_table.t97_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t97_igw.id
# }

# public subnet
resource "aws_subnet" "t97_public_sn" {
  vpc_id                  = aws_vpc.t97_vpc.id
  cidr_block              = cidrsubnet(var.t97_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t97}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t97_public_rt_assoc" {
  subnet_id      = aws_subnet.t97_public_sn.id
  route_table_id = aws_route_table.t97_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t97_private_rt" {
  vpc_id = aws_vpc.t97_vpc.id

  tags = {
    Name = "st_private_rt_${var.t97}"
  }
}

# private subnet
resource "aws_subnet" "t97_private_sn" {
  vpc_id                  = aws_vpc.t97_vpc.id
  cidr_block              = cidrsubnet(var.t97_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t97}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t97_private_rt_assoc" {
  subnet_id      = aws_subnet.t97_private_sn.id
  route_table_id = aws_route_table.t97_private_rt.id
}


### security group

resource "aws_security_group" "t97_public_sg" {

  name        = "t97_public_sg"
  vpc_id      = aws_vpc.t97_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t97}"
  }
}

### compute

resource "aws_instance" "t97_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t97_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t97}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t97_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
###
### t98
###

### networking

# set vpc
resource "aws_vpc" "t98_vpc" {
  cidr_block           = var.t98_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "st_vpc-${var.t98_vpc_name}"
  }
}

# # create IGW per VPC
# resource "aws_internet_gateway" "t98_igw" {
#   vpc_id = aws_vpc.t98_vpc.id

#   tags = {
#     Name = "st_igw-${var.t98_vpc_name}"
#   }
# }

# create RT for Public subnets
resource "aws_route_table" "t98_public_rt" {
  vpc_id = aws_vpc.t98_vpc.id

  tags = {
    Name = "st_public_rt_${var.t98}"
  }
}

# # generate DF in public RT with NH IGW
# resource "aws_route" "t98_default_route" {
#   route_table_id         = aws_route_table.t98_public_rt.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.t98_igw.id
# }

# public subnet
resource "aws_subnet" "t98_public_sn" {
  vpc_id                  = aws_vpc.t98_vpc.id
  cidr_block              = cidrsubnet(var.t98_cidr, var.sbit, 0)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "st_vpc-${var.t98}_public_sn"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "t98_public_rt_assoc" {
  subnet_id      = aws_subnet.t98_public_sn.id
  route_table_id = aws_route_table.t98_public_rt.id
}

### private subnet is not used yet

# create RT for Private subnets
resource "aws_route_table" "t98_private_rt" {
  vpc_id = aws_vpc.t98_vpc.id

  tags = {
    Name = "st_private_rt_${var.t98}"
  }
}

# private subnet
resource "aws_subnet" "t98_private_sn" {
  vpc_id                  = aws_vpc.t98_vpc.id
  cidr_block              = cidrsubnet(var.t98_cidr, var.sbit, 1)
  availability_zone       = element(data.aws_availability_zones.azs.names, 0)
  map_public_ip_on_launch = false
  tags = {
    Name = "st_vpc-${var.t98}_private_sn"
  }
}

# map private subnets to private RT
resource "aws_route_table_association" "t98_private_rt_assoc" {
  subnet_id      = aws_subnet.t98_private_sn.id
  route_table_id = aws_route_table.t98_private_rt.id
}


### security group

resource "aws_security_group" "t98_public_sg" {

  name        = "t98_public_sg"
  vpc_id      = aws_vpc.t98_vpc.id
  description = "iperf-endpoint"

  ingress = [
    {
      description      = "permit all traffic inbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  egress = [
    {
      description      = "permit all traffic outbound"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]
  tags = {
    Name = "st_iperf_${var.t98}"
  }
}

### compute

resource "aws_instance" "t98_iperf-endpoint" {
  instance_type               = var.instance_type
  ami                         = data.aws_ami.iperf_ami.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.t98_public_sn.id

  tags = {
    Name = "st_iperf-t${var.t98}-${random_integer.random.id}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [aws_security_group.t98_public_sg.id]

  root_block_device {
    volume_size = var.vol_size
  }
}
# ###
# ### t99
# ###

# ### networking

# # set vpc
# resource "aws_vpc" "t99_vpc" {
#   cidr_block           = var.t99_cidr
#   instance_tenancy     = "default"
#   enable_dns_support   = true
#   enable_dns_hostnames = true
#   tags = {
#     Name = "st_vpc-${var.t99_vpc_name}"
#   }
# }

# # # create IGW per VPC
# # resource "aws_internet_gateway" "t99_igw" {
# #   vpc_id = aws_vpc.t99_vpc.id

# #   tags = {
# #     Name = "st_igw-${var.t99_vpc_name}"
# #   }
# # }

# # create RT for Public subnets
# resource "aws_route_table" "t99_public_rt" {
#   vpc_id = aws_vpc.t99_vpc.id

#   tags = {
#     Name = "st_public_rt_${var.t99}"
#   }
# }

# # # generate DF in public RT with NH IGW
# # resource "aws_route" "t99_default_route" {
# #   route_table_id         = aws_route_table.t99_public_rt.id
# #   destination_cidr_block = "0.0.0.0/0"
# #   gateway_id             = aws_internet_gateway.t99_igw.id
# # }

# # public subnet
# resource "aws_subnet" "t99_public_sn" {
#   vpc_id                  = aws_vpc.t99_vpc.id
#   cidr_block              = cidrsubnet(var.t99_cidr, var.sbit, 0)
#   availability_zone       = element(data.aws_availability_zones.azs.names, 0)
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "st_vpc-${var.t99}_public_sn"
#   }
# }

# # map public subnets to public RT
# resource "aws_route_table_association" "t99_public_rt_assoc" {
#   subnet_id      = aws_subnet.t99_public_sn.id
#   route_table_id = aws_route_table.t99_public_rt.id
# }

# ### private subnet is not used yet

# # create RT for Private subnets
# resource "aws_route_table" "t99_private_rt" {
#   vpc_id = aws_vpc.t99_vpc.id

#   tags = {
#     Name = "st_private_rt_${var.t99}"
#   }
# }

# # private subnet
# resource "aws_subnet" "t99_private_sn" {
#   vpc_id                  = aws_vpc.t99_vpc.id
#   cidr_block              = cidrsubnet(var.t99_cidr, var.sbit, 1)
#   availability_zone       = element(data.aws_availability_zones.azs.names, 0)
#   map_public_ip_on_launch = false
#   tags = {
#     Name = "st_vpc-${var.t99}_private_sn"
#   }
# }

# # map private subnets to private RT
# resource "aws_route_table_association" "t99_private_rt_assoc" {
#   subnet_id      = aws_subnet.t99_private_sn.id
#   route_table_id = aws_route_table.t99_private_rt.id
# }


# ### security group

# resource "aws_security_group" "t99_public_sg" {

#   name        = "t99_public_sg"
#   vpc_id      = aws_vpc.t99_vpc.id
#   description = "iperf-endpoint"

#   ingress = [
#     {
#       description      = "permit all traffic inbound"
#       from_port        = 0
#       to_port          = 0
#       protocol         = "-1"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = []
#       prefix_list_ids  = []
#       security_groups  = []
#       self             = false
#     }
#   ]
#   egress = [
#     {
#       description      = "permit all traffic outbound"
#       from_port        = 0
#       to_port          = 0
#       protocol         = "-1"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = []
#       prefix_list_ids  = []
#       security_groups  = []
#       self             = false
#     }
#   ]
#   tags = {
#     Name = "st_iperf_${var.t99}"
#   }
# }

# ### compute

# resource "aws_instance" "t99_iperf-endpoint" {
#   instance_type               = var.instance_type
#   ami                         = data.aws_ami.iperf_ami.id
#   associate_public_ip_address = true
#   subnet_id                   = aws_subnet.t99_public_sn.id

#   tags = {
#     Name = "st_iperf-t${var.t99}-${random_integer.random.id}"
#   }

#   key_name               = aws_key_pair.st_auth.id
#   vpc_security_group_ids = [aws_security_group.t99_public_sg.id]

#   root_block_device {
#     volume_size = var.vol_size
#   }
# }
# ###
# ### t100
# ###

# ### networking

# # set vpc
# resource "aws_vpc" "t100_vpc" {
#   cidr_block           = var.t100_cidr
#   instance_tenancy     = "default"
#   enable_dns_support   = true
#   enable_dns_hostnames = true
#   tags = {
#     Name = "st_vpc-${var.t100_vpc_name}"
#   }
# }

# # # create IGW per VPC
# # resource "aws_internet_gateway" "t100_igw" {
# #   vpc_id = aws_vpc.t100_vpc.id

# #   tags = {
# #     Name = "st_igw-${var.t100_vpc_name}"
# #   }
# # }

# # create RT for Public subnets
# resource "aws_route_table" "t100_public_rt" {
#   vpc_id = aws_vpc.t100_vpc.id

#   tags = {
#     Name = "st_public_rt_${var.t100}"
#   }
# }

# # # generate DF in public RT with NH IGW
# # resource "aws_route" "t100_default_route" {
# #   route_table_id         = aws_route_table.t100_public_rt.id
# #   destination_cidr_block = "0.0.0.0/0"
# #   gateway_id             = aws_internet_gateway.t100_igw.id
# # }

# # public subnet
# resource "aws_subnet" "t100_public_sn" {
#   vpc_id                  = aws_vpc.t100_vpc.id
#   cidr_block              = cidrsubnet(var.t100_cidr, var.sbit, 0)
#   availability_zone       = element(data.aws_availability_zones.azs.names, 0)
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "st_vpc-${var.t100}_public_sn"
#   }
# }

# # map public subnets to public RT
# resource "aws_route_table_association" "t100_public_rt_assoc" {
#   subnet_id      = aws_subnet.t100_public_sn.id
#   route_table_id = aws_route_table.t100_public_rt.id
# }

# ### private subnet is not used yet

# # create RT for Private subnets
# resource "aws_route_table" "t100_private_rt" {
#   vpc_id = aws_vpc.t100_vpc.id

#   tags = {
#     Name = "st_private_rt_${var.t100}"
#   }
# }

# # private subnet
# resource "aws_subnet" "t100_private_sn" {
#   vpc_id                  = aws_vpc.t100_vpc.id
#   cidr_block              = cidrsubnet(var.t100_cidr, var.sbit, 1)
#   availability_zone       = element(data.aws_availability_zones.azs.names, 0)
#   map_public_ip_on_launch = false
#   tags = {
#     Name = "st_vpc-${var.t100}_private_sn"
#   }
# }

# # map private subnets to private RT
# resource "aws_route_table_association" "t100_private_rt_assoc" {
#   subnet_id      = aws_subnet.t100_private_sn.id
#   route_table_id = aws_route_table.t100_private_rt.id
# }


# ### security group

# resource "aws_security_group" "t100_public_sg" {

#   name        = "t100_public_sg"
#   vpc_id      = aws_vpc.t100_vpc.id
#   description = "iperf-endpoint"

#   ingress = [
#     {
#       description      = "permit all traffic inbound"
#       from_port        = 0
#       to_port          = 0
#       protocol         = "-1"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = []
#       prefix_list_ids  = []
#       security_groups  = []
#       self             = false
#     }
#   ]
#   egress = [
#     {
#       description      = "permit all traffic outbound"
#       from_port        = 0
#       to_port          = 0
#       protocol         = "-1"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = []
#       prefix_list_ids  = []
#       security_groups  = []
#       self             = false
#     }
#   ]
#   tags = {
#     Name = "st_iperf_${var.t100}"
#   }
# }

# ### compute

# resource "aws_instance" "t100_iperf-endpoint" {
#   instance_type               = var.instance_type
#   ami                         = data.aws_ami.iperf_ami.id
#   associate_public_ip_address = true
#   subnet_id                   = aws_subnet.t100_public_sn.id

#   tags = {
#     Name = "st_iperf-t${var.t100}-${random_integer.random.id}"
#   }

#   key_name               = aws_key_pair.st_auth.id
#   vpc_security_group_ids = [aws_security_group.t100_public_sg.id]

#   root_block_device {
#     volume_size = var.vol_size
#   }
# }