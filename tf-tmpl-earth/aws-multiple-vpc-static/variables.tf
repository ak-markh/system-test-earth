# --- root/variables.tf ---

### env
variable "project" {}

### region
variable "aws_region" {
  default = "us-east-2"
  type    = string
}

### security 
variable "access_ip" {}
variable "public_key_path" {}
variable "key_name" {}

### shared - networking
variable "sbit" {
  description = "add sbit to parent cidr block to get subnet cidr block size"
  default = "1"
  type    = string
}

### shared - compute
variable "instance_type" {
  description = "EC2 instance size"
  default = "t3.small"
  type    = string
}
variable "vol_size" {
  description = "volume size"
  default = "20"
  type    = string
}

### per tenant

# --- t91/variables ---
# t91

variable "t91_vpc_name" {
  description = "tenant vpc name"
  default = "t91"
  type    = string
}
variable "t91" {
  description = "tenant name"
  default = "91"
  type    = string
}
variable "t91_cidr" {
  description = "tenant name"
  default = "10.91.0.0/16"
  type    = string
}
# --- t92/variables ---
# t92

variable "t92_vpc_name" {
  description = "tenant vpc name"
  default = "t92"
  type    = string
}
variable "t92" {
  description = "tenant name"
  default = "92"
  type    = string
}
variable "t92_cidr" {
  description = "tenant name"
  default = "10.92.0.0/17"
  type    = string
}
# --- t93/variables ---
# t93

variable "t93_vpc_name" {
  description = "tenant vpc name"
  default = "t93"
  type    = string
}
variable "t93" {
  description = "tenant name"
  default = "93"
  type    = string
}
variable "t93_cidr" {
  description = "tenant name"
  default = "10.93.0.0/18"
  type    = string
}
# --- t94/variables ---
# t94

variable "t94_vpc_name" {
  description = "tenant vpc name"
  default = "t94"
  type    = string
}
variable "t94" {
  description = "tenant name"
  default = "94"
  type    = string
}
variable "t94_cidr" {
  description = "tenant name"
  default = "10.94.0.0/19"
  type    = string
}
# --- t95/variables ---
# t95

variable "t95_vpc_name" {
  description = "tenant vpc name"
  default = "t95"
  type    = string
}
variable "t95" {
  description = "tenant name"
  default = "95"
  type    = string
}
variable "t95_cidr" {
  description = "tenant name"
  default = "10.95.0.0/20"
  type    = string
}
# --- t96/variables ---
# t96

variable "t96_vpc_name" {
  description = "tenant vpc name"
  default = "t96"
  type    = string
}
variable "t96" {
  description = "tenant name"
  default = "96"
  type    = string
}
variable "t96_cidr" {
  description = "tenant name"
  default = "10.96.0.0/21"
  type    = string
}
# --- t97/variables ---
# t97

variable "t97_vpc_name" {
  description = "tenant vpc name"
  default = "t97"
  type    = string
}
variable "t97" {
  description = "tenant name"
  default = "97"
  type    = string
}
variable "t97_cidr" {
  description = "tenant name"
  default = "10.97.0.0/22"
  type    = string
}
# --- t98/variables ---
# t98

variable "t98_vpc_name" {
  description = "tenant vpc name"
  default = "t98"
  type    = string
}
variable "t98" {
  description = "tenant name"
  default = "98"
  type    = string
}
variable "t98_cidr" {
  description = "tenant name"
  default = "10.98.0.0/23"
  type    = string
}
# # --- t99/variables ---
# # t99

# variable "t99_vpc_name" {
#   description = "tenant vpc name"
#   default = "t99"
#   type    = string
# }
# variable "t99" {
#   description = "tenant name"
#   default = "99"
#   type    = string
# }
# variable "t99_cidr" {
#   description = "tenant name"
#   default = "10.99.0.0/24"
#   type    = string
# }
# # --- t100/variables ---
# # t100

# variable "t100_vpc_name" {
#   description = "tenant vpc name"
#   default = "t100"
#   type    = string
# }
# variable "t100" {
#   description = "tenant name"
#   default = "100"
#   type    = string
# }
# variable "t100_cidr" {
#   description = "tenant name"
#   default = "10.100.0.0/25"
#   type    = string
# }