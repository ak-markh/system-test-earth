# --- root/locals.tf ---

locals {
  # if needed, override the default aws region
  region = "us-east-2"
}
