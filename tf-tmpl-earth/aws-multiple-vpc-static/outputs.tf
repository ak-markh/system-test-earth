# --- t91/outputs ---

output "t91_vpc_id" {
  value = aws_vpc.t91_vpc.id
}
output "t91_iperf-endpoint_Name" {
  value = "${aws_instance.t91_iperf-endpoint.tags.Name}"
}
output "t91_iperf-endpoint_az" {
  value = aws_instance.t91_iperf-endpoint.availability_zone
}
output "t91_iperf-endpoint_private_ipv4" {
  value = aws_instance.t91_iperf-endpoint.private_ip
}
output "t91_iperf-endpoint_public_ipv4" {
  value = aws_instance.t91_iperf-endpoint.public_ip
}
# --- t92/outputs ---

output "t92_vpc_id" {
  value = aws_vpc.t92_vpc.id
}
output "t92_iperf-endpoint_Name" {
  value = "${aws_instance.t92_iperf-endpoint.tags.Name}"
}
output "t92_iperf-endpoint_az" {
  value = aws_instance.t92_iperf-endpoint.availability_zone
}
output "t92_iperf-endpoint_private_ipv4" {
  value = aws_instance.t92_iperf-endpoint.private_ip
}
output "t92_iperf-endpoint_public_ipv4" {
  value = aws_instance.t92_iperf-endpoint.public_ip
}
# --- t93/outputs ---

output "t93_vpc_id" {
  value = aws_vpc.t93_vpc.id
}
output "t93_iperf-endpoint_Name" {
  value = "${aws_instance.t93_iperf-endpoint.tags.Name}"
}
output "t93_iperf-endpoint_az" {
  value = aws_instance.t93_iperf-endpoint.availability_zone
}
output "t93_iperf-endpoint_private_ipv4" {
  value = aws_instance.t93_iperf-endpoint.private_ip
}
output "t93_iperf-endpoint_public_ipv4" {
  value = aws_instance.t93_iperf-endpoint.public_ip
}
# --- t94/outputs ---

output "t94_vpc_id" {
  value = aws_vpc.t94_vpc.id
}
output "t94_iperf-endpoint_Name" {
  value = "${aws_instance.t94_iperf-endpoint.tags.Name}"
}
output "t94_iperf-endpoint_az" {
  value = aws_instance.t94_iperf-endpoint.availability_zone
}
output "t94_iperf-endpoint_private_ipv4" {
  value = aws_instance.t94_iperf-endpoint.private_ip
}
output "t94_iperf-endpoint_public_ipv4" {
  value = aws_instance.t94_iperf-endpoint.public_ip
}
# --- t95/outputs ---

output "t95_vpc_id" {
  value = aws_vpc.t95_vpc.id
}
output "t95_iperf-endpoint_Name" {
  value = "${aws_instance.t95_iperf-endpoint.tags.Name}"
}
output "t95_iperf-endpoint_az" {
  value = aws_instance.t95_iperf-endpoint.availability_zone
}
output "t95_iperf-endpoint_private_ipv4" {
  value = aws_instance.t95_iperf-endpoint.private_ip
}
output "t95_iperf-endpoint_public_ipv4" {
  value = aws_instance.t95_iperf-endpoint.public_ip
}
# --- t96/outputs ---

output "t96_vpc_id" {
  value = aws_vpc.t96_vpc.id
}
output "t96_iperf-endpoint_Name" {
  value = "${aws_instance.t96_iperf-endpoint.tags.Name}"
}
output "t96_iperf-endpoint_az" {
  value = aws_instance.t96_iperf-endpoint.availability_zone
}
output "t96_iperf-endpoint_private_ipv4" {
  value = aws_instance.t96_iperf-endpoint.private_ip
}
output "t96_iperf-endpoint_public_ipv4" {
  value = aws_instance.t96_iperf-endpoint.public_ip
}
# --- t97/outputs ---

output "t97_vpc_id" {
  value = aws_vpc.t97_vpc.id
}
output "t97_iperf-endpoint_Name" {
  value = "${aws_instance.t97_iperf-endpoint.tags.Name}"
}
output "t97_iperf-endpoint_az" {
  value = aws_instance.t97_iperf-endpoint.availability_zone
}
output "t97_iperf-endpoint_private_ipv4" {
  value = aws_instance.t97_iperf-endpoint.private_ip
}
output "t97_iperf-endpoint_public_ipv4" {
  value = aws_instance.t97_iperf-endpoint.public_ip
}
# --- t98/outputs ---

output "t98_vpc_id" {
  value = aws_vpc.t98_vpc.id
}
output "t98_iperf-endpoint_Name" {
  value = "${aws_instance.t98_iperf-endpoint.tags.Name}"
}
output "t98_iperf-endpoint_az" {
  value = aws_instance.t98_iperf-endpoint.availability_zone
}
output "t98_iperf-endpoint_private_ipv4" {
  value = aws_instance.t98_iperf-endpoint.private_ip
}
output "t98_iperf-endpoint_public_ipv4" {
  value = aws_instance.t98_iperf-endpoint.public_ip
}
# # --- t99/outputs ---

# output "t99_vpc_id" {
#   value = aws_vpc.t99_vpc.id
# }
# output "t99_iperf-endpoint_Name" {
#   value = "${aws_instance.t99_iperf-endpoint.tags.Name}"
# }
# output "t99_iperf-endpoint_az" {
#   value = aws_instance.t99_iperf-endpoint.availability_zone
# }
# output "t99_iperf-endpoint_private_ipv4" {
#   value = aws_instance.t99_iperf-endpoint.private_ip
# }
# output "t99_iperf-endpoint_public_ipv4" {
#   value = aws_instance.t99_iperf-endpoint.public_ip
# }
# # --- t100/outputs ---

# output "t100_vpc_id" {
#   value = aws_vpc.t100_vpc.id
# }
# output "t100_iperf-endpoint_Name" {
#   value = "${aws_instance.t100_iperf-endpoint.tags.Name}"
# }
# output "t100_iperf-endpoint_az" {
#   value = aws_instance.t100_iperf-endpoint.availability_zone
# }
# output "t100_iperf-endpoint_private_ipv4" {
#   value = aws_instance.t100_iperf-endpoint.private_ip
# }
# output "t100_iperf-endpoint_public_ipv4" {
#   value = aws_instance.t100_iperf-endpoint.public_ip
# }