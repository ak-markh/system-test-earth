terraform {
  required_version = ">= 0.14.0"

  required_providers {
    alkira = {
      source  = "alkiranet/alkira"
      version = ">= 0.6.3"
    }
  }
}
