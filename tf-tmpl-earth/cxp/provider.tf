provider "aws" {
  region     = "us-east-2"
  access_key = var.access_key
  secret_key = var.secret_key
}

provider "alkira" {
  portal   = var.alkira_portal
  username = var.alkira_username
  password = var.alkira_password
}
