variable "access_key" {
  description = "AWS - access key"
  type        = string
  sensitive   = true
}
variable "secret_key" {
  description = "AWS - secret key"
  type        = string
  sensitive   = true
}
variable "alkira_portal" {
  description = "Alkira (CSX) - portal)"
  type        = string
}
variable "alkira_username" {
  description = "Alkira - user"
  type        = string
}
variable "alkira_password" {
  description = "Alkira - password"
  type        = string
}
variable "aws_credential_id" {
  description = "AWS - credential on Alkira portal"
  type        = string
}
variable "aws_account_id" {
  description = "AWS account id"
  type        = string
}
variable "aws_region" {
  description = "AWS region"
  type        = string
}
variable "cxp_region" {
  description = "CXP region"
  type        = string
}

