resource "alkira_connector_aws_vpc" "aws_seg9_vpc97" {
  name           = "aws_seg9_vpc97"
  vpc_id         = "vpc-005f951de02325fe2"
  vpc_cidr       = ["10.91.0.0/16"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-04e86700140776cd1"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc98" {
  name           = "aws_seg9_vpc98"
  vpc_id         = "vpc-00b513d6cf3725ead"
  vpc_cidr       = ["10.92.0.0/17"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-0815c0a0ad005b120"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc99" {
  name           = "aws_seg9_vpc99"
  vpc_id         = "vpc-0d44c90a95ea512be"
  vpc_cidr       = ["10.93.0.0/18"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-010fdb6a990160c01"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc100" {
  name           = "aws_seg9_vpc100"
  vpc_id         = "vpc-0f5e059a1b26bea11"
  vpc_cidr       = ["10.94.0.0/19"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-0ab805aee625a636e"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc101" {
  name           = "aws_seg9_vpc101"
  vpc_id         = "vpc-0c78d20f1e28ece9a"
  vpc_cidr       = ["10.95.0.0/20"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-066718b743c6b29af"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc102" {
  name           = "aws_seg9_vpc102"
  vpc_id         = "vpc-0e1ef648586cace17"
  vpc_cidr       = ["10.96.0.0/21"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-04b2d8fbd39013968"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc103" {
  name           = "aws_seg9_vpc103"
  vpc_id         = "vpc-0080628110b7507b3"
  vpc_cidr       = ["10.97.0.0/22"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-0413df83ae7988aca"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc104" {
  name           = "aws_seg9_vpc104"
  vpc_id         = "vpc-05c8ff39ac01a098c"
  vpc_cidr       = ["172.16.16.0/23"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-09930c4ced492bb39"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc105" {
  name           = "aws_seg9_vpc105"
  vpc_id         = "vpc-0ff00bce4e5b2f4db"
  vpc_cidr       = ["192.168.91.0/24"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-013ab96123dff12ef"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc106" {
  name           = "aws_seg9_vpc106"
  vpc_id         = "vpc-0365ee30c3ba21032"
  vpc_cidr       = ["192.168.92.0/25"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-0bc21259187e47db2"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc107" {
  name           = "aws_seg9_vpc107"
  vpc_id         = "vpc-02a3a929d3ecfd582"
  vpc_cidr       = ["192.168.93.0/26"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-064e92e4ce60a820f"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
resource "alkira_connector_aws_vpc" "aws_seg9_vpc108" {
  name           = "aws_seg9_vpc108"
  vpc_id         = "vpc-024f18fc18b5642b9"
  vpc_cidr       = ["192.168.94.0/27"]
  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region
  credential_id  = var.aws_credential_id
  cxp            = var.cxp_region
  group          = "east-aws"
  segment_id     = "266"
  size           = "SMALL"
  vpc_route_table {
    id      = "rtb-08fcdd7b97cb7b5a3"
    options = "ADVERTISE_DEFAULT_ROUTE"
  }
}
