# --- root/variables.tf ---
variable "aws_region" {
  default = "us-east-2"
  type    = string
}

variable "access_ip" {}
