# --- root/main.tf --- 

#Deploy AWS resources used by System Test 

# NOTE: please see "providers.tf" to change the AWS region

# NETWORK main block
module "networking" {
  source           = "./networking"
  vpc_cidr         = local.vpc_cidr
  private_sn_count = 1
  public_sn_count  = 1
  private_cidrs    = [for i in range(1, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
  public_cidrs     = [for i in range(2, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
  max_subnets      = 20
  access_ip        = var.access_ip
  security_groups  = local.security_groups
}

# COMPUTE main block
module "compute" {
  source          = "./compute"
  public_sg       = module.networking.public_sg
  public_subnets  = module.networking.public_subnets
  instance_count  = 2
  instance_type   = "t3.small"
  vol_size        = "20"
  public_key_path = "/home/ubuntu/.ssh/st-earth.pub"
  key_name        = "st-earth"
}
