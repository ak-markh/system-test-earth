# --- root/locals.tf ---
locals {
  vpc_cidr = "10.111.0.0/16"
  security_groups = {
    public = {
      name        = "public_sg"
      description = "access from DEV VPN"
      ingress = {
        ssh = {
          from        = 22
          to          = 22
          protocol    = "tcp"
          cidr_blocks = [var.access_ip]
        }
      }
    }
  }
}
