# --- root/outputs.tf ---
output "all_instances_public_v4" {
  value = { for i in module.compute.single_instance : i.tags.Name => "${i.public_ip}" }
}
