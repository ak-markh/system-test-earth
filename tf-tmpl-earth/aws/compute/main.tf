# ---- st compute/main.tf -----

# use the iperf image
data "aws_ami" "server_ami" {
  most_recent = true

  owners = ["695888640573"]

  filter {
    name   = "name"
    values = ["iperf-ami"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

# create a unique hostname
resource "random_id" "st_node_id" {
  byte_length = 2
  count       = var.instance_count
  keepers = {
    key_name = var.key_name
  }
}

# specify the SSH key name and file location
resource "aws_key_pair" "st_auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

# create the compute instance
resource "aws_instance" "st_node" {
  count         = var.instance_count
  instance_type = var.instance_type
  ami           = data.aws_ami.server_ami.id

  tags = {
    Name = "st_node-${random_id.st_node_id[count.index].dec}"
  }

  key_name               = aws_key_pair.st_auth.id
  vpc_security_group_ids = [var.public_sg]
  subnet_id              = var.public_subnets[count.index]

  root_block_device {
    volume_size = var.vol_size
  }
}
