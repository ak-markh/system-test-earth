# --- networking/outputs.tf ---

output "vpc_id" {
  value = aws_vpc.st_vpc.id
}

output "public_sg" {
  value = aws_security_group.st_sg["public"].id
}

output "public_subnets" {
  value = aws_subnet.st_public_subnet.*.id
}
