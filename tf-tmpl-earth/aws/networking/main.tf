# ---- networking/main.tf -----

# get list of az per region
data "aws_availability_zones" "available" {}

# generate integer. used for names
resource "random_integer" "random" {
  min = 1
  max = 100
}

# set az
resource "random_shuffle" "az_list" {
  input        = data.aws_availability_zones.available.names
  result_count = var.max_subnets
}

# create VPC
resource "aws_vpc" "st_vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform   = "true"
    Environment = "earth"
    Owner       = "markh"
    Name        = "st_vpc-${random_integer.random.id}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

# create public subnet
resource "aws_subnet" "st_public_subnet" {
  count                   = var.public_sn_count
  vpc_id                  = aws_vpc.st_vpc.id
  cidr_block              = var.public_cidrs[count.index]
  map_public_ip_on_launch = true
  availability_zone       = random_shuffle.az_list.result[count.index]

  tags = {
    Name = "st_public_${count.index + 1}"
  }
}

# create private subnet
resource "aws_subnet" "st_private_subnet" {
  count                   = var.private_sn_count
  vpc_id                  = aws_vpc.st_vpc.id
  cidr_block              = var.private_cidrs[count.index]
  map_public_ip_on_launch = false
  availability_zone       = random_shuffle.az_list.result[count.index]

  tags = {
    Name = "st_private_${count.index + 1}"
  }
}

# create IGW per VPC
resource "aws_internet_gateway" "st_internet_gateway" {
  vpc_id = aws_vpc.st_vpc.id

  tags = {
    Name = "st_igw"
  }
}

# create RT for Public subnets
resource "aws_route_table" "st_public_rt" {
  vpc_id = aws_vpc.st_vpc.id

  tags = {
    Name = "st_public"
  }
}

# generate DF in public RT with NH IGW
resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.st_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.st_internet_gateway.id
}

# create RT for Private subnets. default option
resource "aws_default_route_table" "st_private_rt" {
  default_route_table_id = aws_vpc.st_vpc.default_route_table_id

  tags = {
    Name = "st_private"
  }
}

# map public subnets to public RT
resource "aws_route_table_association" "st_public_assoc" {
  count          = var.public_sn_count
  subnet_id      = aws_subnet.st_public_subnet.*.id[count.index]
  route_table_id = aws_route_table.st_public_rt.id
}

# create SG for VPC
resource "aws_security_group" "st_sg" {
  for_each    = var.security_groups
  name        = each.value.name
  description = each.value.description
  vpc_id      = aws_vpc.st_vpc.id

  #public Security Group
  dynamic "ingress" {
    for_each = each.value.ingress
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
